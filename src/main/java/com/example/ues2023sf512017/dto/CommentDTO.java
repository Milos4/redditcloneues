package com.example.ues2023sf512017.dto;

import java.util.Date;

import com.example.ues2023sf512017.model.Comment;

public class CommentDTO {
    private Long id;
    private String text;
    private Date timestamp;
    private boolean isDeleted;
    private Long postId;
    private String user;

    public CommentDTO() {
    }

    public CommentDTO(Long id, String text, Date timestamp, boolean isDeleted, Long postId, String user) {
        this.id = id;
        this.text = text;
        this.timestamp = timestamp;
        this.isDeleted = isDeleted;
        this.postId = postId;
        this.user = user;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
    
	public static CommentDTO fromEntity(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setText(comment.getText());
        commentDTO.setTimestamp(comment.getTimestamp());
        commentDTO.setDeleted(comment.isDeleted());
        commentDTO.setPostId(comment.getPost().getId());
        commentDTO.setUser(comment.getUser().getUsername());
        return commentDTO;
    }

}