package com.example.ues2023sf512017.dto;

import java.util.Date;
import java.util.List;

public class UserDTO {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String avatar;
    private Date registrationDate;
    private String description;
    private String displayName;
//    private List<PostDTO> posts;
//    private List<CommunityDTO> communities;
    
    public UserDTO() {
    }

    public UserDTO(Long id, String username, String password, String email, String avatar, Date registrationDate,
			String description, String displayName) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
		this.registrationDate = registrationDate;
		this.description = description;
		this.displayName = displayName;
//		this.posts = posts;
	//	this.communities = communities;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

    
    
}