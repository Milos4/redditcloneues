package com.example.ues2023sf512017.dto;

import java.util.Date;

public class CommunityDTO {
    private Long id;
    private String name;
    private String description;
    private Date creationDate;
    private boolean isSuspended;
    private String suspendedReason;
    private String communityPDFPath;
    private Long userId;

    public CommunityDTO() {
    }

    public CommunityDTO(Long id, String name, String description, Date creationDate, boolean isSuspended, String suspendedReason, String communityPDFPath, Long userId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.isSuspended = isSuspended;
        this.suspendedReason = suspendedReason;
        this.communityPDFPath = communityPDFPath;
        this.userId = userId;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isSuspended() {
		return isSuspended;
	}

	public void setSuspended(boolean isSuspended) {
		this.isSuspended = isSuspended;
	}

	public String getSuspendedReason() {
		return suspendedReason;
	}

	public void setSuspendedReason(String suspendedReason) {
		this.suspendedReason = suspendedReason;
	}

	public String getCommunityPDFPath() {
		return communityPDFPath;
	}

	public void setCommunityPDFPath(String communityPDFPath) {
		this.communityPDFPath = communityPDFPath;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

    
}





