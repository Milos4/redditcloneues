package com.example.ues2023sf512017.dto;

import java.util.Date;
import java.util.List;

import com.example.ues2023sf512017.model.Comment;
import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.User;

public class PostDTO {

        private Long id;
        private String title;
        private String text;
        private Date creationDate;
        private String postPDFPath;
        private String descriptionPDF;
        private UserDTO user;
        private CommunityDTO community;
        private List<CommentDTO> comments;
        private List<ReactionDTO> reactions;
        
        
        public PostDTO() {
        	
        }
        
		public PostDTO(Long id, String title, String text, Date creationDate, String postPDFPath, String descriptionPDF,
				UserDTO user, CommunityDTO community, List<CommentDTO> comments, List<ReactionDTO> reactions) {
			super();
			this.id = id;
			this.title = title;
			this.text = text;
			this.creationDate = creationDate;
			this.postPDFPath = postPDFPath;
			this.descriptionPDF = descriptionPDF;
			this.user = user;
			this.community = community;
			this.comments = comments;
			this.reactions = reactions;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public Date getCreationDate() {
			return creationDate;
		}
		public void setCreationDate(Date creationDate) {
			this.creationDate = creationDate;
		}
		public String getPostPDFPath() {
			return postPDFPath;
		}
		public void setPostPDFPath(String postPDFPath) {
			this.postPDFPath = postPDFPath;
		}
		public String getDescriptionPDF() {
			return descriptionPDF;
		}
		public void setDescriptionPDF(String descriptionPDF) {
			this.descriptionPDF = descriptionPDF;
		}

		
		public UserDTO getUser() {
			return user;
		}

		public void setUser(UserDTO user) {
			this.user = user;
		}

		public CommunityDTO getCommunity() {
			return community;
		}

		public void setCommunity(CommunityDTO community) {
			this.community = community;
		}

		public List<CommentDTO> getComments() {
			return comments;
		}

		public void setComments(List<CommentDTO> comments) {
			this.comments = comments;
		}

		public List<ReactionDTO> getReactions() {
			return reactions;
		}

		public void setReactions(List<ReactionDTO> reactions) {
			this.reactions = reactions;
		}
	
        
        
        
        
}