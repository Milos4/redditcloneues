package com.example.ues2023sf512017.dto;

import java.util.Date;

import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.ReactionType;

public class ReactionDTO {
    private Long id;
    private ReactionType reactionType;
    private Date timestamp;
    private Long postId;
    private Long commentId;
    private Long userId;

    public ReactionDTO() {
    }

    public ReactionDTO(Long id, ReactionType reactionType, Date timestamp, Long postId, Long commentId, Long userId) {
        this.id = id;
        this.reactionType = reactionType;
        this.timestamp = timestamp;
        this.postId = postId;
        this.commentId = commentId;
        this.userId = userId;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReactionType getReactionType() {
		return reactionType;
	}

	public void setReactionType(ReactionType reactionType) {
		this.reactionType = reactionType;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public static ReactionDTO fromEntity(Reaction reaction) {
        ReactionDTO reactionDTO = new ReactionDTO();
        reactionDTO.setId(reaction.getId());
        reactionDTO.setReactionType(reaction.getReactionType());
        reactionDTO.setTimestamp(reaction.getTimestamp());
        
        if (reaction.getPost() != null) {
            reactionDTO.setPostId(reaction.getPost().getId());
        }
        
        if (reaction.getComment() != null) {
            reactionDTO.setCommentId(reaction.getComment().getId());
        }
        
        if (reaction.getUser() != null) {
            reactionDTO.setUserId(reaction.getUser().getId());
        }
        
        return reactionDTO;
    }
}
