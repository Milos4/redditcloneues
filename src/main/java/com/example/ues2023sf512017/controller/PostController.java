package com.example.ues2023sf512017.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.io.File;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.ues2023sf512017.dto.CommunityDTO;
import com.example.ues2023sf512017.dto.PostDTO;
import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.repository.CommunityRepository;
import com.example.ues2023sf512017.repository.PostRepository;
import com.example.ues2023sf512017.service.PostService;
import com.example.ues2023sf512017.service.ReactionService;
import com.example.ues2023sf512017.service.UserService;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;
    private final ReactionService reactionService;
    private final RestHighLevelClient elasticsearchClient;
    private final UserService userService;
    private final PostRepository postRepository;
    private final CommunityRepository communityRepository;

    
    @Value("${upload.dir}")
    private String uploadDir; 

    public PostController(PostService postService,ReactionService reactionService,RestHighLevelClient elasticsearchClient, UserService userService,
    		PostRepository postRepository,CommunityRepository communityRepository) {
        this.postService = postService;
        this.reactionService = reactionService;
        this.elasticsearchClient = elasticsearchClient;
        this.userService = userService;
        this.postRepository = postRepository;
        this.communityRepository = communityRepository;
    }

    @GetMapping
    public List<PostDTO> getAllPosts() {
    	return postService.getAllPosts();
    }  
    
    @GetMapping("/{postId}/reactions")
    public ResponseEntity<List<Reaction>> getReactionsForPost(@PathVariable Long postId) {
        List<Reaction> reactions = reactionService.getReactionsForPost(postId);
        if (reactions == null) {
            return ResponseEntity.ok(new ArrayList<>());
        }
        return ResponseEntity.ok(reactions);
    }
    
    
    @PutMapping("/{postId}")
    public ResponseEntity<PostDTO> updatePost(@PathVariable Long postId, @RequestBody PostDTO postDTO) {
        PostDTO updatedPost = postService.updatePost(postId, postDTO);
        return ResponseEntity.ok(updatedPost);
    }



    @DeleteMapping("/{postId}")
    public ResponseEntity<String> deletePost(@PathVariable Long postId) {
        try {
            postService.deletePost(postId);
            return ResponseEntity.ok("Post je uspješno obrisan.");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Došlo je do greške pri brisanju posta.");
        }
    }
     
	 @GetMapping("/search")
	 public ResponseEntity<List<PostDTO>> searchCommunities(@RequestParam(required = false) String title,                                                       @RequestParam(required = false) String text) {
	     List<PostDTO> postDTOs;
	     if (title != null) {
	    	 postDTOs = postService.searchCommunitiesByTitle(title);
	     } else if (text != null) {
	    	 postDTOs = postService.searchCommunitiesByText(text);
	     } else {
	         return ResponseEntity.badRequest().build();
	     }
	     return ResponseEntity.ok(postDTOs);
	 }
	 
	 @PostMapping("/{communityId}")
	 public ResponseEntity<String> createPost(
	     @PathVariable Long communityId,
	     @RequestParam("user") String user,
	     @RequestParam("title") String title,
	     @RequestParam("text") String text,
	     @RequestParam("pdfFile") MultipartFile pdfFile,
	     @RequestParam("descriptionPDF") String descriptionPDF
	 ) {
	     try {
	         User u = userService.findByUsername(user);
	         Community community = communityRepository.findById(communityId).orElse(null);
	         Post post = new Post();
	         post.setUser(u);
	         post.setCommunity(community);
	         post.setTitle(title);
	         post.setText(text);
	         Calendar calendar = Calendar.getInstance();
	         Date currentDate = calendar.getTime();
	         post.setCreationDate(currentDate);
	         post.setDescriptionPDF(descriptionPDF);
	         if (!pdfFile.isEmpty()) {
	                File directory = new File(uploadDir);
	                if (!directory.exists()) {
	                    directory.mkdirs();
	                }
	                String fileName = System.currentTimeMillis() + "_" + pdfFile.getOriginalFilename();

	                File destFile = new File(uploadDir + File.separator + fileName);
	                pdfFile.transferTo(destFile);

	                post.setPostPDFPath(fileName);
	            }
	         postRepository.save(post);

	         return ResponseEntity.ok("Post je uspešno kreiran.");
	     } catch (Exception e) {
	         e.printStackTrace();
	         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Došlo je do greške pri kreiranju posta.");
	     }
	 }
    
}
    
