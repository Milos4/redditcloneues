package com.example.ues2023sf512017.controller;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.ues2023sf512017.dto.CommunityDTO;
import com.example.ues2023sf512017.dto.PostDTO;
import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.repository.CommunityRepository;
import com.example.ues2023sf512017.service.CommunityService;
import com.example.ues2023sf512017.service.PostService;
import com.example.ues2023sf512017.service.UserService;

@RestController
@RequestMapping("/api/communities")
public class CommunityController {

	   
    @Value("${upload.dir}")
    private String uploadDir; 

	 @Autowired
	 private CommunityService communityService;
	 @Autowired
	 private  UserService userService;
	 @Autowired
	 private PostService postService;
	 @Autowired
	 private CommunityRepository communityRepository;

//	 @PostMapping
//	 public ResponseEntity<String> createCommunity(@RequestBody CommunityDTO communityDTO) {
//		communityService.createCommunity(communityDTO);
//	    return ResponseEntity.ok("Community created successfully");
//	}
	 
	 @PutMapping("/{communityId}")
	 public ResponseEntity<CommunityDTO> updateCommunity(@PathVariable Long communityId, @RequestBody CommunityDTO updatedCommunityDTO) {
	     CommunityDTO updatedCommunity = communityService.updateCommunity(communityId, updatedCommunityDTO);
	     return ResponseEntity.ok(updatedCommunity);
	 }
	 

	 
	 @DeleteMapping("/{communityId}")
	    public ResponseEntity<String> deleteCommunity(@PathVariable Long communityId) {
	        try {
	   	     	communityService.deleteCommunity(communityId);
	            return ResponseEntity.ok("Zajednica je uspješno obrisana.");
	        } catch (Exception e) {
	            e.printStackTrace();
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Došlo je do greške pri brisanju zajednice.");
	        }
	    }
	 
	 @GetMapping("/search")
	 public ResponseEntity<List<CommunityDTO>> searchCommunities(@RequestParam(required = false) String name,                                                         @RequestParam(required = false) String description) {
	     List<CommunityDTO> communityDTOs; 
	     if (name != null) {
	         communityDTOs = communityService.searchCommunitiesByName(name);
	     } else if (description != null) {
	         communityDTOs = communityService.searchCommunitiesByDescription(description);
	     } else {
	         return ResponseEntity.badRequest().build();
	     }
	     return ResponseEntity.ok(communityDTOs);
	 }
	 
	 @GetMapping
	    public List<Community> getAllCommunity() {
	    	return communityService.getAll();
	    }
	 @GetMapping("/{id}")
	    public ResponseEntity<Community> getCommunityById(@PathVariable Long id) {
	        Community community = communityService.getCommunityById(id);

	        if (community == null) {
	            return ResponseEntity.notFound().build();
	        }

	        return ResponseEntity.ok(community);
	    }
	 
	 @GetMapping("/{communityId}/posts")
	 public ResponseEntity<List<PostDTO>> getPostsByCommunity(@PathVariable Long communityId) {
	     List<PostDTO> posts = postService.getPostsByCommunityId(communityId);
	     return ResponseEntity.ok(posts);
	 }

	 
	 @PostMapping
	 public ResponseEntity<String> createPost(
	     @RequestParam("user") String user,
	     @RequestParam("name") String title,
	     @RequestParam("communityPDFPath") MultipartFile communityPDFPath,
	     @RequestParam("description") String description
	 ) {
	     try {
	         User u = userService.findByUsername(user);
	         Community community = new Community();
	         community.setModerator(u);
	         community.setName(title);
	         community.setDescription(description);;
	         Calendar calendar = Calendar.getInstance();
	         Date currentDate = calendar.getTime();
	         community.setCreationDate(currentDate);
	         if (!communityPDFPath.isEmpty()) {
	                File directory = new File(uploadDir);
	                if (!directory.exists()) {
	                    directory.mkdirs();
	                }
	                String fileName = System.currentTimeMillis() + "_" + communityPDFPath.getOriginalFilename();

	                File destFile = new File(uploadDir + File.separator + fileName);
	                communityPDFPath.transferTo(destFile);

	                community.setCommunityPDFPath(fileName);
	            }
	         communityRepository.save(community);

	         return ResponseEntity.ok("Komjuniti je uspešno kreiran.");
	     } catch (Exception e) {
	         e.printStackTrace();
	         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Došlo je do greške pri kreiranju komjunitija.");
	     }
	 }
	 
}