package com.example.ues2023sf512017.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ues2023sf512017.model.ReactionType;
import com.example.ues2023sf512017.service.ReactionService;

@RestController
@RequestMapping("/api/reactions")
public class ReactionController {

	private final ReactionService reactionService;

	public ReactionController(ReactionService reactionService) {
		this.reactionService = reactionService;
	}
	
	@PostMapping("/{postId}")
    public ResponseEntity<String> toggleReaction(
        @PathVariable Long postId,
        @RequestParam("username") String username,
        @RequestParam("reactionType") ReactionType reactionType
    ) {
        try {
            reactionService.toggleReaction(postId, reactionType,username);
            return ResponseEntity.ok("Reaction toggled successfully.");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Error toggling reaction: " + e.getMessage());
        }
    }
	
	
	
}