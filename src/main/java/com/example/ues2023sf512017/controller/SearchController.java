package com.example.ues2023sf512017.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.ues2023sf512017.model.CommunitySearchResult;
import com.example.ues2023sf512017.model.PostSearchResult;
import com.example.ues2023sf512017.model.SearchRequest;
import com.example.ues2023sf512017.service.PostElasticsearchService;


@RestController
@RequestMapping("/api")
public class SearchController {

    @Autowired
    private PostElasticsearchService postElasticsearchService;

    @PostMapping("/search")
    public ResponseEntity<List<PostSearchResult>> searchPosts(@RequestBody SearchRequest searchRequest) {
        String query = searchRequest.getQuery();
        String option = searchRequest.getOption();
        

        List<PostSearchResult> results = postElasticsearchService.searchPosts(query, option);

        return ResponseEntity.ok(results);
    }
    
    @PostMapping("/search-communities")
    public ResponseEntity<List<CommunitySearchResult>> searchCommunities(@RequestBody SearchRequest searchRequest) {
        String query = searchRequest.getQuery();
        String option = searchRequest.getOption();
        

        List<CommunitySearchResult> results = postElasticsearchService.searchCommunities(query, option);

        return ResponseEntity.ok(results);
    }
}