package com.example.ues2023sf512017.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ues2023sf512017.service.PostElasticsearchService;
import com.example.ues2023sf512017.service.UserService;
import com.example.ues2023sf512017.dto.LoginDTO;
import com.example.ues2023sf512017.dto.RegistrationDTO;
import com.example.ues2023sf512017.model.Admin;
import com.example.ues2023sf512017.model.User;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class LoginController {

    private final UserService userService;
    private PostElasticsearchService indexingService;

    
    public LoginController(UserService userService,PostElasticsearchService indexingService) {
        this.userService = userService;
        this.indexingService = indexingService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegistrationDTO registrationDTO) {
        if (!userService.isUsernameAvailable(registrationDTO.getUsername())) {
            return ResponseEntity.badRequest().body("Korisničko ime je već zauzeto.");
        }
        if (!userService.isEmailAvailable(registrationDTO.getEmail())) {
            return ResponseEntity.badRequest().body("Email adresa je već registrirana.");
        }
        
        String password = registrationDTO.getPassword();
        String confirmPassword = registrationDTO.getConfirmPassword();

        if (!password.equals(confirmPassword)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Passwords do not match");
        }
        
        User user = new User();
        user.setUsername(registrationDTO.getUsername());
        user.setPassword(registrationDTO.getPassword());
        user.setEmail(registrationDTO.getEmail());
        user.setDescription(registrationDTO.getDescription());
        user.setDisplayName(registrationDTO.getDisplayName());
        user.setAvatar("");
        Date currentDate = new Date();
        user.setRegistrationDate(currentDate);
   
        userService.registerUser(user);
        
        return ResponseEntity.ok("Registracija uspješna.");
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginDTO loginDTO) {
       
        User user = userService.findByUsername(loginDTO.getUsername());  
        if (user == null || !userService.validatePassword(user, loginDTO.getPassword())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Neuspješna prijava. Provjerite korisničko ime i lozinku.");
        }
        
        
        indexingService.indexPostsIntoElasticsearch();
        indexingService.indexCommunitiesIntoElasticsearch();

        return ResponseEntity.ok("Prijava uspješna.");
    }
}