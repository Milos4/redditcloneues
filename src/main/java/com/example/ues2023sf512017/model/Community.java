package com.example.ues2023sf512017.model;


import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "communities")
public class Community {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Column
    private String description;
    
    @Column(name = "creation_date", nullable = false)
    private Date creationDate;
    
    @Column(name = "is_suspended")
    private boolean isSuspended;
    
    @Column(name = "suspended_reason")
    private String suspendedReason;
    
    @Column(name = "community_pdf_path")
    private String communityPDFPath;
     
    @ManyToOne
    @JoinColumn(name = "moderator_id")
    private User  user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isSuspended() {
		return isSuspended;
	}

	public void setSuspended(boolean isSuspended) {
		this.isSuspended = isSuspended;
	}

	public String getSuspendedReason() {
		return suspendedReason;
	}

	public void setSuspendedReason(String suspendedReason) {
		this.suspendedReason = suspendedReason;
	}

	public String getCommunityPDFPath() {
		return communityPDFPath;
	}

	public void setCommunityPDFPath(String communityPDFPath) {
		this.communityPDFPath = communityPDFPath;
	}
	public User  getModerator() {
		return user;
	}

	public void setModerator(User  user) {
		this.user = user;
	}
    
    
    
}