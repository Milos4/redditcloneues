package com.example.ues2023sf512017.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String title;
    
    @Column(nullable = false)
    private String text;
    
    @Column(name = "creation_date", nullable = false)
    private Date creationDate;
    
    @Column(name = "post_pdf_path")
    private String postPDFPath;
    
    @Column(name = "description_pdf")
    private String descriptionPDF;
    
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    @ManyToOne
    @JoinColumn(name = "community_id", nullable = false)
    private Community community;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getPostPDFPath() {
		return postPDFPath;
	}

	public void setPostPDFPath(String postPDFPath) {
		this.postPDFPath = postPDFPath;
	}

	public String getDescriptionPDF() {
		return descriptionPDF;
	}

	public void setDescriptionPDF(String descriptionPDF) {
		this.descriptionPDF = descriptionPDF;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Community getCommunity() {
		return community;
	}

	public void setCommunity(Community community) {
		this.community = community;
	}


}