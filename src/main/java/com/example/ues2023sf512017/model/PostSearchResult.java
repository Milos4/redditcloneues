package com.example.ues2023sf512017.model;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "posts")
public class PostSearchResult {
    private Long id;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String title;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String text;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String descriptionPdf;
    
    
    public PostSearchResult() {
    	
    }
    
	public PostSearchResult(Long id, String title, String text, String descriptionPdf) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.descriptionPdf = descriptionPdf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDescriptionPdf() {
		return descriptionPdf;
	}

	public void setDescriptionPdf(String descriptionPdf) {
		this.descriptionPdf = descriptionPdf;
	}
   
    
}

