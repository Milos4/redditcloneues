package com.example.ues2023sf512017.model;

public enum ReactionType {
    UPVOTE,
    DOWNVOTE
}