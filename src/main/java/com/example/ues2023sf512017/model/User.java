package com.example.ues2023sf512017.model;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.DiscriminatorType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "user_type", discriminatorType = DiscriminatorType.STRING)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, unique = true)
    private String username;
    
    @Column(nullable = false)
    private String password;
    
    @Column(nullable = false, unique = true)
    private String email;
    
    @Column
    private String avatar;
    
    @Column(name = "registration_date", nullable = false)
    private Date registrationDate;
    
    @Column
    private String description;
    
	@Column(name = "display_name")
    private String displayName;
    
   
	public User() {
		
	}
	
	public User(String username, String password, String email, String avatar, Date registrationDate, String description, String displayName) {
	    this.username = username;
	    this.password = password;
	    this.email = email;
	    this.avatar = avatar;
	    this.registrationDate = registrationDate;
	    this.description = description;
	    this.displayName = displayName;
	}
	
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

        
	
}