package com.example.ues2023sf512017.model;

public class SearchRequest {
    private String query;
    private String option;
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}

   
}