package com.example.ues2023sf512017.model;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "communities")
public class CommunitySearchResult {
	
    private Long id;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String title;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String description;
    
    @Field(type = FieldType.Text, analyzer = "serbian_analyzer")
    private String communityPDFPath;
    
    private Double averageKarma;

    private Long numberOfPosts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCommunityPDFPath() {
		return communityPDFPath;
	}

	public void setCommunityPDFPath(String communityPDFPath) {
		this.communityPDFPath = communityPDFPath;
	}

	public Double getAverageKarma() {
		return averageKarma;
	}

	public void setAverageKarma(Double averageKarma) {
		this.averageKarma = averageKarma;
	}

	public Long getNumberOfPosts() {
		return numberOfPosts;
	}

	public void setNumberOfPosts(Long numberOfPosts) {
		this.numberOfPosts = numberOfPosts;
	} 
    
    
}
