package com.example.ues2023sf512017;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ues2023sf512017Application {

	public static void main(String[] args) {
		SpringApplication.run(Ues2023sf512017Application.class, args);
	}

}
