package com.example.ues2023sf512017.preprocessing;

import java.text.Normalizer;

import org.springframework.stereotype.Component;

@Component
public class PreprocessQuery {
	
	public String preprocessQuery(String query) {
	    // Remove diacritical marks
	    String normalizedQuery = Normalizer.normalize(query, Normalizer.Form.NFD)
	            .replaceAll("\\p{M}", "");
	    
	    // Convert to lowercase
	    normalizedQuery = normalizedQuery.toLowerCase();
	    
	    // Remove non-alphanumeric characters
	    normalizedQuery = normalizedQuery.replaceAll("[^a-z0-9\\s]", "");
	    
	    // Remove extra whitespaces
	    normalizedQuery = normalizedQuery.trim().replaceAll("\\s+", " ");
	    
	    return normalizedQuery;
	}

}
