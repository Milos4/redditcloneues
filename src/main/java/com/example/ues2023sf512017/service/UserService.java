package com.example.ues2023sf512017.service;

import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.dto.UserDTO;
import com.example.ues2023sf512017.model.Admin;
import com.example.ues2023sf512017.model.Moderator;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.repository.UserRepository;

@Service
public class UserService {
	
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }
    
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    
    public boolean validatePassword(User user, String password) {
        String storedPassword = user.getPassword();
        return password.equals(storedPassword);
    }
    
    public void registerUser(User user) {
        userRepository.save(user);
    }

    public boolean isAdmin(User user) {
        return user instanceof Admin;
    }
    
    public boolean isModerator(User user) {
        return user instanceof Moderator;
    }
    
    public boolean isUsernameAvailable(String username) {
        User existingUser = userRepository.findByUsername(username);    
        return existingUser == null;
    }
    public boolean isEmailAvailable(String email) {
        User existingEmail = userRepository.findByEmail(email);    
        return existingEmail == null;
    }
    
    public static UserDTO toUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        userDTO.setAvatar(user.getAvatar());
        userDTO.setRegistrationDate(user.getRegistrationDate());
        userDTO.setDescription(user.getDescription());
        userDTO.setDisplayName(user.getDisplayName());
        // Convert communities if needed
        // Convert posts if needed
        return userDTO;
    }
    

}