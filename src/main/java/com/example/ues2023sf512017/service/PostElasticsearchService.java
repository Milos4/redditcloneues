package com.example.ues2023sf512017.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.PostSearchResult;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.ReactionType;
import com.example.ues2023sf512017.model.CommunitySearchResult;
import com.example.ues2023sf512017.repository.CommunityRepository;
import com.example.ues2023sf512017.repository.PostRepository;
import com.example.ues2023sf512017.repository.ReactionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostElasticsearchService {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;
    private final RestHighLevelClient elasticsearchClient;
    private final PostRepository postRepository;
    private final ObjectMapper objectMapper;
    private final CommunityRepository communityRepository;
    private final ReactionRepository reactionRepository;

    @Autowired
    public PostElasticsearchService(
        RestHighLevelClient elasticsearchClient,
        PostRepository postRepository,
        CommunityRepository communityRepository,
        ObjectMapper objectMapper,
        ReactionRepository reactionRepository,
        ElasticsearchRestTemplate elasticsearchRestTemplate
    ) {
        this.elasticsearchClient = elasticsearchClient;
        this.postRepository = postRepository;
        this.objectMapper = objectMapper;
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.communityRepository = communityRepository;
        this.reactionRepository = reactionRepository;

    }
    
    public List<PostSearchResult> searchPosts(String query, String option) {
    	 NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
    		        .withQuery(QueryBuilders.boolQuery()
    		            .should(QueryBuilders.matchPhraseQuery(option, query))  
    		        )
    		        .build();

    		    SearchHits<PostSearchResult> searchHits = elasticsearchRestTemplate.search(
    		        searchQuery,
    		        PostSearchResult.class
    		    );

    		    return searchHits.stream()
    		        .map(SearchHit::getContent)
    		        .collect(Collectors.toList());
    		};
    
    		 public List<CommunitySearchResult> searchCommunities(String query, String option) {
    		        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
    		            .withQuery(QueryBuilders.boolQuery()
    		                .should(QueryBuilders.matchPhraseQuery(option, query))
    		            )
    		            .build();

    		        SearchHits<CommunitySearchResult> searchHits = elasticsearchRestTemplate.search(
    		            searchQuery,
    		            CommunitySearchResult.class
    		        );

    		        List<CommunitySearchResult> results = searchHits.stream()
    		            .map(SearchHit::getContent)
    		            .collect(Collectors.toList());

    		        for (CommunitySearchResult result : results) {
    		            double averageKarma = calculateAverageKarmaForCommunity(result.getId());
    		            int numberOfPosts = calculateNumberOfPostsForCommunity(result.getId());

    		            result.setAverageKarma(averageKarma);
    		            result.setNumberOfPosts(Long.valueOf(numberOfPosts));
    		        }

    		        return results;
    		    }
    		
    		
    		
    public void indexPostsIntoElasticsearch() {
        List<Post> postsFromDatabase = postRepository.findAll(); 

        for (Post post : postsFromDatabase) {
            PostSearchResult searchResult = new PostSearchResult();
            searchResult.setId(post.getId());
            searchResult.setTitle(post.getTitle());
            searchResult.setText(post.getText());
            searchResult.setDescriptionPdf(post.getDescriptionPDF());

            IndexRequest request = new IndexRequest("posts");
            request.id(String.valueOf(post.getId()));
            try {
                request.source(objectMapper.writeValueAsString(searchResult), XContentType.JSON);
                IndexResponse response = elasticsearchClient.index(request, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void indexCommunitiesIntoElasticsearch() {
        List<Community> communitiesFromDatabase = communityRepository.findAll(); 

        for (Community community : communitiesFromDatabase) {
            CommunitySearchResult searchResult = new CommunitySearchResult();
            searchResult.setId(community.getId());
            searchResult.setTitle(community.getName());
            searchResult.setDescription(community.getDescription()); 
            searchResult.setCommunityPDFPath(community.getCommunityPDFPath());
            IndexRequest request = new IndexRequest("communities");
            request.id(String.valueOf(community.getId()));
            try {
                request.source(objectMapper.writeValueAsString(searchResult), XContentType.JSON);
                IndexResponse response = elasticsearchClient.index(request, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private double calculateAverageKarmaForCommunity(Long communityId) {
        Community community = communityRepository.findById(communityId).orElse(null);
        if (community == null) {
            return 0.0; 
        }
        List<Post> communityPosts = postRepository.findByCommunityId(communityId);  
        if (communityPosts.isEmpty()) {
            return 0.0;
        }
        double totalKarma = 0.0; 
        for (Post post : communityPosts) {
            int postKarma = calculateKarmaForPost(post.getId()); 
            totalKarma += postKarma;
        }
        return totalKarma / communityPosts.size();
    }
    
    
    private int calculateKarmaForPost(Long postId) {
        List<Reaction> postReactions = reactionRepository.findByPostId(postId);
        
        int karma = 0;

        for (Reaction reaction : postReactions) {
            if (reaction.getReactionType() == ReactionType.UPVOTE) {
                karma++; 
            } else if (reaction.getReactionType() == ReactionType.DOWNVOTE) {
                karma--; 
            }
        }
        return karma;
    }

    private int calculateNumberOfPostsForCommunity(Long communityId) {
        Community community = communityRepository.findById(communityId).orElse(null);
        
        if (community == null) {
            return 0;
        }

        List<Post> communityPosts = postRepository.findByCommunityId(communityId);

        return communityPosts.size();
    }
}