package com.example.ues2023sf512017.service;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.dto.CommunityDTO;
import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.preprocessing.PreprocessQuery;
import com.example.ues2023sf512017.repository.CommunityRepository;
import com.example.ues2023sf512017.repository.UserRepository;

@Service
public class CommunityService {
	
    private final CommunityRepository communityRepository;
    private final UserRepository userRepository;
    
    @Autowired
    private PreprocessQuery preprocessQuery;

    public CommunityService(CommunityRepository communityRepository, UserRepository userRepository) {
        this.communityRepository = communityRepository;
        this.userRepository = userRepository;
    }
    
    public List<Community> getAll() {
    	return  communityRepository.findAll();
    }
   
    
    public void createCommunity(CommunityDTO communityDTO) {
        Community community = new Community();
        community.setName(communityDTO.getName());
        community.setDescription(communityDTO.getDescription());
        community.setCreationDate(communityDTO.getCreationDate());
        community.setSuspended(communityDTO.isSuspended());
        community.setSuspendedReason(communityDTO.getSuspendedReason());
        community.setCommunityPDFPath(communityDTO.getCommunityPDFPath());
        User user = userRepository.getById(communityDTO.getUserId());
        community.setModerator(user);
        
        communityRepository.save(community);
	    }
    
    public CommunityDTO updateCommunity(Long communityId, CommunityDTO updatedCommunityDTO) {
    	
        Community existingCommunity = communityRepository.findById(communityId).orElse(null);
        existingCommunity.setName(updatedCommunityDTO.getName());
        existingCommunity.setDescription(updatedCommunityDTO.getDescription());
        
        Community updatedCommunity = communityRepository.save(existingCommunity);
        CommunityDTO communityDTO = new CommunityDTO();
        communityDTO.setName(existingCommunity.getName());
        
        return communityDTO;
    }
    
    public void deleteCommunity(Long communityId) {
        communityRepository.deleteById(communityId);
    }
     
    public List<CommunityDTO> searchCommunitiesByName(String name) {
        String processedName = preprocessQuery.preprocessQuery(name);
        List<Community> communities = communityRepository.findByNameContainingIgnoreCase(processedName);
        return convertToDTOList(communities);
    }

    public List<CommunityDTO> searchCommunitiesByDescription(String description) {
        String processedDescription = preprocessQuery.preprocessQuery(description);
        List<Community> communities = communityRepository.findByDescriptionContainingIgnoreCase(processedDescription);
        return convertToDTOList(communities);
    }

    private List<CommunityDTO> convertToDTOList(List<Community> communities) {
        List<CommunityDTO> communityDTOs = new ArrayList<>();
        for (Community community : communities) {
            CommunityDTO communityDTO = new CommunityDTO();
            communityDTO.setName(community.getName());
            communityDTOs.add(communityDTO);
        }
        return communityDTOs;
    }
    
    public static CommunityDTO toCommunityDTO(Community community) {
        CommunityDTO communityDTO = new CommunityDTO();
        communityDTO.setId(community.getId());
        communityDTO.setName(community.getName());
        communityDTO.setDescription(community.getDescription());
        communityDTO.setCreationDate(community.getCreationDate());
        communityDTO.setSuspended(community.isSuspended());
        communityDTO.setSuspendedReason(community.getSuspendedReason());
        communityDTO.setCommunityPDFPath(community.getCommunityPDFPath());
        // Convert posts if needed
        // Convert moderator if needed
        return communityDTO;
    }
    
    public Community getCommunityById(Long id) {
        return communityRepository.findById(id).orElse(null);
    }
    
}