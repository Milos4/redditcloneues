package com.example.ues2023sf512017.service;

import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.repository.CommentRepository;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

}