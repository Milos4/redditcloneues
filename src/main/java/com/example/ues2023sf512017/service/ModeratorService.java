package com.example.ues2023sf512017.service;

import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.repository.ModeratorRepository;

@Service
public class ModeratorService {
    private final ModeratorRepository moderatorRepository;

    public ModeratorService(ModeratorRepository moderatorRepository) {
        this.moderatorRepository = moderatorRepository;
    }

}
