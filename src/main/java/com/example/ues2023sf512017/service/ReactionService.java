package com.example.ues2023sf512017.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.ReactionType;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.repository.PostRepository;
import com.example.ues2023sf512017.repository.ReactionRepository;
import com.example.ues2023sf512017.repository.UserRepository;

@Service
public class ReactionService {
	
    private final PostRepository postRepository;
	private final ReactionRepository reactionRepository;
	private final UserRepository userRepository;
	private final UserService userService;
	private final PostService postService;

    public ReactionService(PostRepository postRepository, ReactionRepository reactionRepository, UserRepository userRepository,UserService userService,PostService postService) {
        this.postRepository = postRepository;
        this.reactionRepository = reactionRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.postService = postService;
    }

    public List<Reaction> getReactionsForPost(Long postId) {
        return reactionRepository.findByPostId(postId);
    }
    public void toggleReaction(Long postId, ReactionType reactionType, String username) {

    	User u = userService.findByUsername(username);
    	Post p = postRepository.findById(postId).orElse(null);
    	
    	Optional<Reaction> existingReaction = reactionRepository.findByPostIdAndUserId(postId, u.getId());
        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        
        if (existingReaction.isPresent()) {
            Reaction reaction = existingReaction.get();
            if (reaction.getReactionType() == reactionType) {
                reactionRepository.delete(reaction);
            } else {
            	reaction.setTimestamp(currentDate);
                reaction.setReactionType(reactionType);
                reactionRepository.save(reaction);
            }
        } else {
            Reaction newReaction = new Reaction();
            newReaction.setTimestamp(currentDate);
            newReaction.setPost(p);
            newReaction.setUser(u);
            newReaction.setReactionType(reactionType);
            reactionRepository.save(newReaction);
        }
    }

}