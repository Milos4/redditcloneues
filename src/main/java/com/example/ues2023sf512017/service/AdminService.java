package com.example.ues2023sf512017.service;

import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.repository.AdminRepository;

@Service
public class AdminService {
    private final AdminRepository adminRepository;

    public AdminService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

}