package com.example.ues2023sf512017.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ues2023sf512017.dto.CommentDTO;
import com.example.ues2023sf512017.dto.CommunityDTO;
import com.example.ues2023sf512017.dto.PostDTO;
import com.example.ues2023sf512017.dto.ReactionDTO;
import com.example.ues2023sf512017.dto.UserDTO;
import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.User;
import com.example.ues2023sf512017.preprocessing.PreprocessQuery;
import com.example.ues2023sf512017.model.Comment;
import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.repository.CommentRepository;
import com.example.ues2023sf512017.repository.CommunityRepository;
import com.example.ues2023sf512017.repository.PostRepository;
import com.example.ues2023sf512017.repository.ReactionRepository;
import com.example.ues2023sf512017.repository.UserRepository;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final CommunityRepository communityRepository;
    private final CommentRepository commentRepository;
    private final ReactionRepository reactionRepository;

    @Autowired
    private PreprocessQuery preprocessQuery;
    
    public PostService(PostRepository postRepository,UserRepository userRepository,CommunityRepository communityRepository, CommentRepository commentRepository, ReactionRepository reactionRepository,  UserService userService) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.communityRepository = communityRepository;
        this.commentRepository = commentRepository;
        this.reactionRepository = reactionRepository;
        this.userService = userService;
    }
    
   
  
    public List<PostDTO> getAllPosts() {
        List<Post> posts = postRepository.findAll();
    
        List<PostDTO> postDTOs = new ArrayList<>();
        
        for (Post post : posts) {
        	User user = userRepository.findById(post.getUser().getId()).orElse(null);
        	Community community = communityRepository.findById(post.getCommunity().getId()).orElse(null);
        	
        	List<Comment> comment = commentRepository.findByPostId(post.getId());
        	List<CommentDTO> commentDTOs = new ArrayList<>();
        	for(Comment c : comment) {
        		CommentDTO commentDTO = CommentDTO.fromEntity(c);
        		commentDTOs.add(commentDTO);
        	}
        	List<Reaction> reaction = reactionRepository.findByPostId(post.getId());
        	List<ReactionDTO> reactionDTOs = new ArrayList<>();
        	for(Reaction r : reaction) {
        		ReactionDTO reactionDTO = ReactionDTO.fromEntity(r);
        		reactionDTOs.add(reactionDTO);
        	}
            PostDTO postDTO = convertToDTO(post);
            UserDTO userDTO = UserService.toUserDTO(user);
            postDTO.setUser(userDTO);
            CommunityDTO communityDTO = CommunityService.toCommunityDTO(community);
            postDTO.setCommunity(communityDTO);
            postDTO.setComments(commentDTOs);
            postDTO.setReactions(reactionDTOs);
            postDTOs.add(postDTO);
        }
        return postDTOs;
    }
    
    public List<PostDTO> getPostsByCommunityId(Long communityId) {
        List<Post> posts = postRepository.findByCommunityId(communityId);

        List<PostDTO> postDTOs = new ArrayList<>();

        for (Post post : posts) {
        	User user = userRepository.findById(post.getUser().getId()).orElse(null);
        	Community community = communityRepository.findById(post.getCommunity().getId()).orElse(null);
        	List<Comment> comment = commentRepository.findByPostId(post.getId());
        	List<CommentDTO> commentDTOs = new ArrayList<>();
        	for(Comment c : comment) {
        		CommentDTO commentDTO = CommentDTO.fromEntity(c);
        		commentDTOs.add(commentDTO);
        	}
        	List<Reaction> reaction = reactionRepository.findByPostId(post.getId());
        	List<ReactionDTO> reactionDTOs = new ArrayList<>();
        	for(Reaction r : reaction) {
        		ReactionDTO reactionDTO = ReactionDTO.fromEntity(r);
        		reactionDTOs.add(reactionDTO);
        	}
            PostDTO postDTO = convertToDTO(post);
            UserDTO userDTO = UserService.toUserDTO(user);
            postDTO.setUser(userDTO);
            CommunityDTO communityDTO = CommunityService.toCommunityDTO(community);
            postDTO.setCommunity(communityDTO);
            postDTO.setComments(commentDTOs);
            postDTO.setReactions(reactionDTOs);
            postDTOs.add(postDTO);
        }
        return postDTOs;
    }
    
    
    
    
    public PostDTO createPost(Long communityId, PostDTO postDTO) {
        Community community = communityRepository.findById(communityId).orElse(null);
        Post post = new Post();
        post.setTitle(postDTO.getTitle());
        post.setText(postDTO.getText());
        post.setCommunity(community);
        Date currentDate = new Date();
        post.setCreationDate(currentDate);
        post.setPostPDFPath(postDTO.getPostPDFPath());
        post.setDescriptionPDF(postDTO.getDescriptionPDF());
        User user = userRepository.findById(postDTO.getUser().getId()).orElse(null);
        post.setUser(user);
        Post savedPost = postRepository.save(post);
        return convertToDTO(savedPost);
    }
    
    public PostDTO updatePost(Long postId, PostDTO postDTO) {
        Post existingPost = postRepository.findById(postId).orElse(null);
            existingPost.setTitle(postDTO.getTitle());
            existingPost.setText(postDTO.getText());
            existingPost.setPostPDFPath(postDTO.getPostPDFPath());
            existingPost.setDescriptionPDF(postDTO.getDescriptionPDF());
            Post updatedPost = postRepository.save(existingPost);
            return convertToDTO(updatedPost);
    }
    
    public void deletePost(Long postId) {
        postRepository.deleteById(postId);
    }
    
    public List<PostDTO> searchCommunitiesByTitle(String title) {
        String processedTitle = preprocessQuery.preprocessQuery(title);
        List<Post> posts = postRepository.findByTitleContainingIgnoreCase(processedTitle);
        return convertToDTOList(posts);
    }

    public List<PostDTO> searchCommunitiesByText(String text) {
        String processedText = preprocessQuery.preprocessQuery(text);
        List<Post> posts = postRepository.findByTextContainingIgnoreCase(processedText);
        return convertToDTOList(posts);
    }
    

    private PostDTO convertToDTO(Post post) {
        PostDTO postDTO = new PostDTO();
        postDTO.setId(post.getId());
        postDTO.setTitle(post.getTitle());
        postDTO.setText(post.getText());
        postDTO.setCreationDate(post.getCreationDate());
        postDTO.setPostPDFPath(post.getPostPDFPath());
        postDTO.setDescriptionPDF(post.getDescriptionPDF());
        return postDTO;
    }

    private List<PostDTO> convertToDTOList(List<Post> posts) {
        List<PostDTO> postDTOs = new ArrayList<>();
        for (Post post : posts) {
        	PostDTO postDTO = new PostDTO();
        	postDTO.setTitle(post.getTitle());
        	UserDTO userDTO = UserService.toUserDTO(post.getUser());
        	postDTO.setUser(userDTO);
        	postDTOs.add(postDTO);
        }
        return postDTOs;
    }


   
}