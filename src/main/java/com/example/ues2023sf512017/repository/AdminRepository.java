package com.example.ues2023sf512017.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ues2023sf512017.model.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

}