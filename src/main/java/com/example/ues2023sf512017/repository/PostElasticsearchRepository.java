package com.example.ues2023sf512017.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.example.ues2023sf512017.model.PostSearchResult;

public interface PostElasticsearchRepository extends ElasticsearchRepository<PostSearchResult, Long> {
}