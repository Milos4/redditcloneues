package com.example.ues2023sf512017.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
	
	List<Post> findByUserId(Long userId);
	    
	List<Post> findByCommunityId(Long communityId);
	    
	List<Post> findByUserIdAndCommunityId(Long userId, Long communityId);
	 
	List<Post> findByCreationDateGreaterThanEqual(Date date);

	@Query("SELECT p FROM Post p WHERE LOWER(p.title) LIKE %:title%")
    List<Post> findByTitleContainingIgnoreCase(@Param("title") String title);
    
    @Query("SELECT p FROM Post p WHERE LOWER(p.text) LIKE %:text%")
    List<Post> findByTextContainingIgnoreCase(@Param("text") String text);
}
