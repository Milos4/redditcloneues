package com.example.ues2023sf512017.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ues2023sf512017.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
  
    List<Comment> findByPostId(Long postId);
    List<Comment> findByUserId(Long userId);
    List<Comment> findByIsDeletedFalse();
    List<Comment> findByIsDeletedTrue();
}