package com.example.ues2023sf512017.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ues2023sf512017.model.Post;
import com.example.ues2023sf512017.model.Reaction;
import com.example.ues2023sf512017.model.User;

@Repository
public interface ReactionRepository extends JpaRepository<Reaction, Long> {
    
	 List<Reaction> findByPostId(Long postId);
	    
	 List<Reaction> findByCommentId(Long commentId);
	    
	 List<Reaction> findByUserId(Long userId);
	    
	 Optional<Reaction> findByPostIdAndUserId(Long postId, Long userId);
	
	 Reaction findByPostAndUser(Post post, User user);
}