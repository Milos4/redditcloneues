package com.example.ues2023sf512017.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.ues2023sf512017.model.Community;
import com.example.ues2023sf512017.model.User;

@Repository
public interface CommunityRepository extends JpaRepository<Community, Long> {

	
	Community findById(long id);
    List<Community> findByIsSuspendedFalse();
    List<Community> findByIsSuspendedTrue();
    List<Community> findByUserId(Long userid);
    List<Community> findByCreationDateGreaterThanEqual(Date date);
    
    @Query("SELECT c FROM Community c WHERE LOWER(c.name) LIKE %:name%")
    List<Community> findByNameContainingIgnoreCase(@Param("name") String name);
    
    @Query("SELECT c FROM Community c WHERE LOWER(c.description) LIKE %:description%")
    List<Community> findByDescriptionContainingIgnoreCase(@Param("description") String description);
    
}